FROM amd64/node:21

COPY ./node-hostname ./app
WORKDIR ./app
EXPOSE 3000

RUN npm install
CMD ["node", "./bin/www"]