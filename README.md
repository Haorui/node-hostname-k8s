# Kubernetes deploymnet for node-hostname application
Application link: http://k8sv2-1.eit.lth.se:39461/

## Application installation
In order to use ingress, nginx ingress controller needs to be installed in the cluster.

### To install application:
```
cd ./node-hostname-chart
helm -n $NAMESPACE install node-hostname .
```
### To uninstall application:
```
helm -n $NAMESPACE uninstall node-hostname
```


## TODO
### A multicluster solution for powercut downtime problem
1. Install two Kubernetes clusters
2. Intall `Istio` mesh aross two clusters, to enbale fail over to the second cluster when the primary cluster goes down.